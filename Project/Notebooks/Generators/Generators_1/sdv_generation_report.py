import warnings
warnings.filterwarnings("ignore")
import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

from sdv.datasets.demo import download_demo
from sdv.lite import SingleTablePreset
from sdv.evaluation.single_table import run_diagnostic, evaluate_quality, get_column_plot
from sdv.metadata import SingleTableMetadata
from sdv.single_table import CTGANSynthesizer
from sdv.single_table import GaussianCopulaSynthesizer
from sdv.single_table import TVAESynthesizer
from sdv.single_table import CopulaGANSynthesizer


def generate_report_FastML( metadata,real_data, num_rows,synthesizer_name='FAST_ML'): 
    synthesizer = SingleTablePreset(metadata, name=synthesizer_name)
    #Preprocessing assignation 
    synthesizer.fit(data=real_data)
    synthetic_data = synthesizer.sample(num_rows=num_rows)

    diagnostic = run_diagnostic(real_data=real_data, synthetic_data=synthetic_data, metadata=metadata)
    quality_report = evaluate_quality(real_data, synthetic_data, metadata)
    column_shapes = quality_report.get_details('Column Shapes')

    report = {
        'metadata': metadata,
        'diagnostic': diagnostic,
        'quality_report': quality_report,
        'original-Synthetic': {
            'original_data': real_data,
            'synthetic_data': synthetic_data
        }
    }
    return report, synthetic_data

def generate_report_CTGAN(metadata,real_data,samples,enforce_rounding=False,verbose=True):
    synthesizer = CTGANSynthesizer(metadata=metadata,
                                   enforce_rounding=enforce_rounding,
                                        verbose=verbose)
    synthesizer.auto_assign_transformers(real_data)
    synthesizer.get_transformers()
    synthesizer.fit(real_data)
    synthetic_data = synthesizer.sample(samples)
    
    diagnostic = run_diagnostic(real_data=real_data, synthetic_data=synthetic_data, metadata=metadata)
    quality_report = evaluate_quality(real_data, synthetic_data, metadata)
    column_shapes = quality_report.get_details('Column Shapes')
   

    report = {
        'metadata': metadata,
        'diagnostic': diagnostic,
        'quality_report': quality_report,
        'original-Synthetic': {
            'original_data': real_data,
            'synthetic_data': synthetic_data
        }
    }
    return report, synthetic_data

def generate_report_GaussianCopula(metadata,real_data,samples):
    synthesizer = GaussianCopulaSynthesizer(metadata=metadata)
                                            
    synthesizer.fit(real_data)
    synthetic_data = synthesizer.sample(samples)
    return synthetic_data

def generate_report_TVAE(metadata,real_data,samples):
    synthesizer = TVAESynthesizer(metadata=metadata)
    synthesizer.fit(real_data)
    synthetic_data = synthesizer.sample(samples)
    return synthetic_data

def generate_report_CopulaGAN(metadata,real_data,samples):
    synthesizer = CopulaGANSynthesizer(metadata=metadata)
    synthesizer.fit(real_data)
    synthetic_data = synthesizer.sample(samples)
    return synthetic_data

def run_all_models_sdv(data,metadata,samples):
    report_FastML, synthetic_data_FastML = generate_report_FastML(metadata,data,samples)
    report_CTGAN, synthetic_data_CTGAN = generate_report_CTGAN(metadata,data,samples)
    synthetic_data_GaussianCopula = generate_report_GaussianCopula(metadata,data,samples)
    synthetic_data_TVAE = generate_report_TVAE(metadata,data,samples)
    synthetic_data_CopulaGAN = generate_report_CopulaGAN(metadata,data,samples)
    return synthetic_data_FastML, synthetic_data_CTGAN, synthetic_data_GaussianCopula, synthetic_data_TVAE, synthetic_data_CopulaGAN