## This cdode was runned in Google Colab due to the computational cost of the generation of the data
#!pip install smartnoise-synth
#!pip install git+https://github.com/ryan112358/private-pgm.git

import warnings
warnings.filterwarnings("ignore")

import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from snsynth import Synthesizer

    
def AIM_synth(data,epsilon,epsilon_prepro,samples):
    #AIM 
    synthesizer = Synthesizer.create("aim", epsilon=epsilon,verbose=True)
    synthesizer.fit(data, preprocessor_eps=epsilon_prepro)
    aim_samples= synthesizer.sample(samples)
    return aim_samples


def DPCTGAN_synth(data,epsilon,epsilon_prepro,samples,categorical_columns,continuous_columns):
    #DPCTGAN
 
    
    synthesizer = Synthesizer.create("dpctgan", epsilon=epsilon,verbose=True)
    synthesizer.fit(data, preprocessor_eps=epsilon_prepro,
                    categorical_columns=categorical_columns,
                    continuous_columns=continuous_columns)
    DPCTGAN_samples= synthesizer.sample(samples)
    return DPCTGAN_samples


def PATECTGAN_synth(data,epsilon,epsilon_prepro,samples):
    
    #PATECTGAN
    synthesizer = Synthesizer.create("patectgan", epsilon=epsilon,verbose=True)
    synthesizer.fit(data, preprocessor_eps=epsilon_prepro)
    PATECTGAN_samples= synthesizer.sample(samples)
    return PATECTGAN_samples


def PACSYNTH_synth(data,epsilon,epsilon_prepro,samples):
    #PACSYNTH
    synthesizer = Synthesizer.create("pacsynth", epsilon=epsilon,verbose=True)
    synthesizer.fit(data, preprocessor_eps=epsilon_prepro)
    PACSYNTH_samples= synthesizer.sample(samples)
    return PACSYNTH_samples

def MST_synth(data,epsilon,epsilon_prepro,samples):
    #MST
    synthesizer = Synthesizer.create("mst", epsilon=epsilon,verbose=True)
    synthesizer.fit(data, preprocessor_eps=epsilon_prepro)
    MST_samples= synthesizer.sample(samples)
    return MST_samples


def run_all_models_smartnoise(data,epsilon,epsilon_prepro,samples,categorical_columns,continuous_columns):
    aim_samples=AIM_synth(data,epsilon,epsilon_prepro,samples)
    DPCTGAN_samples=DPCTGAN_synth(data,epsilon,epsilon_prepro,samples,categorical_columns,continuous_columns)
    PATECTGAN_samples=PATECTGAN_synth(data,epsilon,epsilon_prepro,samples)
    PACSYNTH_samples=PACSYNTH_synth(data,epsilon,epsilon_prepro,samples)
    MST_samples=MST_synth(data,epsilon,epsilon_prepro,samples)
    return aim_samples,DPCTGAN_samples,PATECTGAN_samples,PACSYNTH_samples,MST_samples