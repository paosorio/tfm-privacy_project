#Main just for running G_epsilon for a given dict jason with the synthetic data
import warnings
warnings.filterwarnings("ignore")


from utils.preprocessing import Preprocessor
from utils.synthetic_epsilon import modeling_epsilon
import pandas as pd 
import os 
import numpy as np 
import snsynth as sn
from utils.G_epsilon import get_Gepsilon_dict
from utils.G_epsilon import plot_gepsilon
from utils.G_epsilon import get_utility_matrix
import json

def main():
    data = pd.read_csv("data/adult_original.csv")
    preprocessor = Preprocessor(data)
    print("Transformers built")
    print("Transforming data")
    tc,tg = preprocessor.transformer_fit()
    print("Data transformed")
    
   
    #Importing a json file
    print("Importing json file")
    with open('results/DPGAN_samples/results_DPGAN.json') as f:
        data = json.load(f)
        f.close()

    data_={}
    for key in data.keys():
        data_[key]=pd.read_json(data[key])
    
    
    #Run the G_epsilon script
    path_output = "results/DPGAN_samples/"
    print(f"Creating output folder in {path_output}")
    os.makedirs(path_output, exist_ok=True)
    data_types=preprocessor.build_dataTypes()
    
     #We are going to generate the utility matrix for the original data in order to save computation time
    print("Generating utility matrix for the original data")
    matrix_Dp_c, matrix_Dp_r = get_utility_matrix(preprocessor.tdata, preprocessor.categorical_columns, 
                                                  preprocessor.continuous_features, data_types)
    
        
    #Calculate the g_epsilon
    print("Calculating g_epsilon")
    gepsilon,mu,delta_t,delta_c,delta_r = get_Gepsilon_dict(path_output,data_,preprocessor.tdata,matrix_Dp_c, matrix_Dp_r,
                                                            preprocessor.continuous_features,preprocessor.categorical_columns,data_types,1,1)
    print("G_epsilon calculated")
    
    plot_gepsilon(gepsilon,mu,delta_t,delta_c,delta_r,'PATEGAN',path_output)
    print("G_epsilon plot saved")   
    

main()
