import warnings
warnings.filterwarnings('ignore')
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import itertools
import os

import sys
sys.path.append('utils/Utility')

import phik
from phik import resources
from phik.binning import bin_data
from phik.report import plot_correlation_matrix
from phik import report
import json

from utility import DataPreProcessor
from utility import train_evaluate_model
from utility_regressor import DataPreProcessor_regressor
from utility_regressor import train_evaluate_model_regressor


#euclidean distance
def euclidean_distance(x1, x2):
    return np.sqrt(np.sum((x1 - x2)**2))

#Getting and plotting phi_k score for matrix 
def get_mu(Ds,Dp, categorical_cols,interval_cols,data_type,path,key):
    #Getting correlation matrix
    phik_DS = Ds.phik_matrix(interval_cols=interval_cols)
    phik_DP = Dp.phik_matrix(interval_cols=interval_cols)
    
    #Saving the correlation matrix as images in a subplot
     
    # if key is not None:
    #     plot_correlation_matrix(phik_DS.values, x_labels=phik_DS.columns, y_labels=phik_DS.index, 
    #                         vmin=0, vmax=1, color_map='Reds', title=r'correlation $\phi_K (D_{s})$'+f' for {key}',
    #                         figsize=(7,5.5))
    #     plt.tight_layout()
        
    #     plt.savefig(f'{path}/phik_DS_{key}.png')

    #     plot_correlation_matrix(phik_DP.values, x_labels=phik_DP.columns, y_labels=phik_DP.index, 
    #                             vmin=0, vmax=1, color_map='Reds', title=r'correlation $\phi_K (D_{p})$',
    #                             figsize=(7,5.5))
    #     plt.tight_layout()
        
    #     plt.savefig(f'{path}/phik_DP.png')
        
        
    
    
    #euclidean norm of the difference between the two matrices
    diff = np.linalg.norm(phik_DS-phik_DP)
    m=phik_DS.shape[0]
    mu = diff/(m*(m-1)/2)
    print(f'The mu is: {mu}')
    return mu
    
    
def get_utility_matrix(D, categorical_cols, interval_cols, data_types):
    
    # m is the number of ML tasks (features in this scenario)
    # K is the number ML models (Classification models)
    # L is the number of scoring functions

    #Using the class from https://github.com/Vicomtech/STDG-evaluation-metrics (By Mikel Hernandez)
    #l = {accuracy,precision,recall,f1,roc_auc}
    #K = {DT,KNN,MLP,RF,SVM}

    #For categorical columns
    #Initialize a 3D matrix with mxKxL
    K_c = 5
    L_c = 4
    m_c = len(categorical_cols)
    matrix_c = np.zeros((K_c,L_c,m_c))
    #Getting a sample of 80% of the data
    data_train = D.sample(frac=0.8, random_state=1)
    data_test = D.drop(data_train.index)

    #only iterate over the categorical columns
    for id,i in enumerate(categorical_cols):
        #The i-th Ml task corresponds to the i-th column in the dataset, 
        #and the label for prediction 
        target_col = i
        model_cols = [col for col in D.columns if col != target_col]
        #new categorical and numerical cols without the target value
        interval_cols_ = [col for col, v in data_types.items() if v=='interval' and col != target_col]
        categorical_cols_ = [col for col, v in data_types.items() if v=='categorical' and col != target_col]

        if len(categorical_cols_) == 0:
            categorical_cols_ = None
        if len(interval_cols_) == 0:
            interval_cols_ = None
        print(f'Model cols: {model_cols}')
        
        print(f'Target column: {target_col}')
        #Model cols are every column except the target column
        if categorical_cols_ is not None:
            categories = [np.array(range(len(D[col].unique()))) for col in categorical_cols_]
        else:
            categories = None
    
        data_preprocessor = DataPreProcessor(categorical_cols_, interval_cols_,
                                            categories)
        
        x_train = data_preprocessor.preprocess_train_data(data_train.loc[:, model_cols])
        y_train = data_train.loc[:, target_col]
        
        x_test = data_preprocessor.preprocess_test_data(data_test.loc[:, model_cols])
        y_test = data_test.loc[:, target_col]
        
        #Checking if there is only one class in the target column
        if len(y_train.unique()) == 1:
            print(f'The target column {target_col} has only one class')
            #Setting the matrix to a perfect score for all models
            matrix_c[:,:,id] = [1,1,1,1]
        else:
            #training RF
            print('Training RF')
            rf_results = train_evaluate_model('RF', x_train, y_train, x_test, y_test)
            rf_results.drop('model',axis=1,inplace=True)
            print(rf_results)
            #training kN
            print('Training KNN')
            knn_results = train_evaluate_model('KNN', x_train, y_train, x_test, y_test)
            knn_results.drop('model',axis=1,inplace=True)
            #training DT
            print('Training DT')
            dt_results = train_evaluate_model('DT', x_train, y_train, x_test, y_test)
            dt_results.drop('model',axis=1,inplace=True)
            #training SVM
            print('Training SVM')
            svm_results = train_evaluate_model('SVM', x_train, y_train, x_test, y_test)
            svm_results.drop('model',axis=1,inplace=True)
            #training MLP
            print('Training MLP')
            mlp_results = train_evaluate_model('MLP', x_train, y_train, x_test, y_test)
            mlp_results.drop('model',axis=1,inplace=True)
            matrix_c[0,:,id]=rf_results.values
            matrix_c[1,:,id]=knn_results.values
            matrix_c[2,:,id]=dt_results.values
            matrix_c[3,:,id]=svm_results.values
            matrix_c[4,:,id]=mlp_results.values
          
    # m is the number of ML tasks (features in this scenario)
    # K is the number ML models (Regressor models)
    # L is the number of scoring functions

    #Using the class from https://github.com/Vicomtech/STDG-evaluation-metrics (By Mikel Hernandez)
    #l = {mean_absolute_error,mean_squared_error,r2}
    #K = {'SVR','RandomForestRegressor','KNeighborsRegressor','MLPRegressor','DecisionTreeRegressor'}


    #For interval columns
    #Initialize a 3D matrix with mxKxL
    K_r = 5
    L_r = 3
    m_r= len(interval_cols)
    matrix_r = np.zeros((K_r,L_r,m_r))
    #Getting a sample of 80% of the data

    #only iterate over the categorical columns
    for id,i in enumerate(interval_cols):
        #The i-th Ml task corresponds to the i-th column in the dataset, 
        #and the label for prediction 
        target_col = i
        model_cols = [col for col in D.columns if col != target_col]
        #new categorical and numerical cols without the target value
        interval_cols_ = [col for col, v in data_types.items() if v=='interval' and col != target_col]
        categorical_cols_ = [col for col, v in data_types.items() if v=='categorical' and col != target_col]

        if len(categorical_cols_) == 0:
            categorical_cols_ = None
        if len(interval_cols_) == 0:
            interval_cols_ = None
            
        print(f'Model cols: {model_cols}')
        print(f'Target column: {target_col}')
        
        #Model cols are every column except the target column
        categories = [np.array(range(len(D[col].unique()))) for col in categorical_cols_]
    
        data_preprocessor = DataPreProcessor(categorical_cols_, interval_cols_,
                                            categories)
        
        x_train = data_preprocessor.preprocess_train_data(data_train.loc[:, model_cols])
        y_train = data_train.loc[:, target_col]
        
        x_test = data_preprocessor.preprocess_test_data(data_test.loc[:, model_cols])
        y_test = data_test.loc[:, target_col]
        #Normalizing the target column 0-1
        y_train = (y_train - y_train.min())/(y_train.max()-y_train.min())
        y_test = (y_test - y_test.min())/(y_test.max()-y_test.min())
           
        #training SVR
        print('Training SVR')
        svr_results = train_evaluate_model_regressor('SVR', x_train, y_train, x_test, y_test)
        svr_results.drop('Model',axis=1,inplace=True)
        print(svr_results)
        #training RandomForestRegressor
        print('Training RandomForestRegressor')
        rf_results = train_evaluate_model_regressor('RandomForestRegressor', x_train, y_train, x_test, y_test)
        rf_results.drop('Model',axis=1,inplace=True)
        print(rf_results)
        #training KNeighborsRegressor
        print('Training KNeighborsRegressor')
        knn_results = train_evaluate_model_regressor('KNeighborsRegressor', x_train, y_train, x_test, y_test)
        knn_results.drop('Model',axis=1,inplace=True)
        print(knn_results)
        #training DecisionTreeRegressor
        print('Training DecisionTreeRegressor')
        dt_results = train_evaluate_model_regressor('DecisionTreeRegressor', x_train, y_train, x_test, y_test)
        dt_results.drop('Model',axis=1,inplace=True)
        print(dt_results)
        #training MLPRegressor
        print('Training MLPRegressor')
        mlp_results = train_evaluate_model_regressor('MLPRegressor', x_train, y_train, x_test, y_test)
        mlp_results.drop('Model',axis=1,inplace=True)
        print(mlp_results)
        matrix_r[0,:,id]=svr_results.values
        matrix_r[1,:,id]=rf_results.values
        matrix_r[2,:,id]=knn_results.values
        matrix_r[3,:,id]=dt_results.values
        matrix_r[4,:,id]=mlp_results.values
        
    return matrix_c, matrix_r

def get_delta(Ds,Dp, matrix_Dp_c, matrix_Dp_r,data_types,categorical_cols,interval_cols):
    #Getting the utility matrix for the synthetic dataset
    matrix_Ds_c, matrix_Ds_r = get_utility_matrix(Ds, categorical_cols, interval_cols, data_types)
      
    #For delta_regressor
    K_r,L_r,m_r,  = matrix_Ds_r.shape
    summ=0
    for m in range(m_r):
        sumk=0
        for k in range(K_r):
            sumL=0
            for l in range(L_r):
                sumL+=euclidean_distance(matrix_Ds_r[k,l,m],matrix_Dp_r[k,l,m])
            sumk+=sumL
        summ+=sumk
        
    delta_l = (1/(m_r*K_r*L_r))*summ 
    print(f'delta value is {delta_l}')
    
    #For delta classifier
    K_c,L_c,m_c,  = matrix_Ds_c.shape
    summ=0  
    for m in range(m_c):
        sumk=0
        for k in range(K_c):
            sumL=0
            for l in range(L_c):
                sumL+=euclidean_distance(matrix_Ds_c[k,l,m],matrix_Dp_c[k,l,m])
            sumk+=sumL
        summ+=sumk
        
    delta_c = (1/(m_c*K_c*L_c))*summ
    print(f'delta value is {delta_c}')
    
    #The final delta is a ponderation according to the number of categorical and interval columns. 
    lambda1= len(categorical_cols)/(len(categorical_cols)+len(interval_cols))
    lambda2= len(interval_cols)/(len(categorical_cols)+len(interval_cols))
    delta_t= lambda1*delta_c+lambda2*delta_l    
    
    return delta_t, delta_c, delta_l  


#Getting epsilon

def get_Gepsilon(Ds,Dp,matrix_Dp_c, matrix_Dp_r ,interval_cols,categorical_cols,data_types,alpha,beta,path, key=None):
    mu = get_mu(Ds,Dp,categorical_cols,interval_cols,data_types,path,key)  
    delta_t, delta_c, delta_r = get_delta(Ds,Dp,matrix_Dp_c, matrix_Dp_r,data_types,categorical_cols,interval_cols)
    G_epsilon=alpha*mu+beta*delta_t
    return G_epsilon,mu,delta_t,delta_c,delta_r




#Getting mu for a dictionary of datasets
def get_mu_dict(data_,Dp,categorical_cols,interval_cols,data_types,path_output):
    mu = {}
    for key in data_.keys():
        mu[key] = get_mu(data_[key],Dp,categorical_cols,interval_cols,data_types,path_output,key)
    
    #Saving the dictionary as a json file
    with open(os.path.join(path_output, 'results_mu.json'), 'w') as f:
        json.dump(mu, f)
        
    return mu


#Plotting mu for a dictionary of datasets
def plot_mu(mu,algorithm,path_output):
    plt.figure(figsize=(10,5))
    plt.plot([float(key.split('_')[-1]) for key in mu.keys()], mu.values(), 'o-')
    plt.title(f'mu for {algorithm}')
    plt.xlabel(r'$\epsilon$')
    plt.ylabel('mu')
    #Saving the figure
    plt.savefig(f'{path_output}/mu_plot.png')


#Getting epsilon for a dictionary of datasets

def get_Gepsilon_dict(path_output,data_,Dp,matrix_Dp_c, matrix_Dp_r ,interval_cols,categorical_cols,data_types,alpha,beta):
    gepsilon = {}
    mu ={}
    delta_t={}
    delta_c={}
    delta_r={}
    for key in data_.keys():
        gepsilon[key],mu[key],delta_t[key],delta_c[key],delta_r[key] = get_Gepsilon( data_[key],Dp, matrix_Dp_c, matrix_Dp_r ,interval_cols, categorical_cols,data_types,alpha,beta,path_output,key)
    
    
    #Saving the dictionaries as a json files
    variables = {
        'gepsilon': gepsilon,
        'mu': mu,
        'delta_t': delta_t,
        'delta_c': delta_c,
        'delta_r': delta_r
    }

    for variable_name, variable_data in variables.items():
        with open(os.path.join(path_output, f"results_{variable_name}.json"), 'w') as f:
            json.dump(variable_data, f)
            
        
    return gepsilon,mu,delta_t,delta_c,delta_r





def plot_gepsilon(gepsilon,mu,delta_t,delta_c,delta_r,algorithm,path_output):
    variables = {
        'gepsilon': gepsilon,
        'mu': mu,
        'delta_t': delta_t,
        'delta_c': delta_c,
        'delta_r': delta_r
    }
    
    for variable_name, variable_data in variables.items():
        plt.figure(figsize=(10,5))
        plt.plot([float(key.split('_')[-1]) for key in variable_data.keys()], variable_data.values(), 'o-')
        plt.title(f'{variable_name} for {algorithm}')
        plt.xlabel(r'$\epsilon$')
        plt.ylabel(f'{variable_name}')
        #Saving the figure
        plt.savefig(f'{path_output}/{variable_name}_plot.png')    
    

    
