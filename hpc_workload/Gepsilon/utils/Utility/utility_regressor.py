## Class inspired by https://github.com/Vicomtech/STDG-evaluation-metrics

#import libraries

import warnings
warnings.filterwarnings("ignore")
import numpy as np
import pandas as pd
import seaborn
from matplotlib import pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, LabelBinarizer


class DataPreProcessor_regressor :
    """
    A class used to preprocess train and test data.

    ...

    Attributes
    ----------
    numerical_columns : list
        a list with the numerical columns names of the dataframe
    categorical_columns : list
        a list with the categorical columns names of the dataframe
    categories : list
        a list of arrays especifying the categories of each categorical attribute to one-hot encode categorical attributes
    label_encoders : list
        a list of LabelEncoders for the categorical attributes
    num_scaler : sklearn.preprocessing.StandardScaler
        a StandardScaler to scale numerical attributes
    onehot_encoder : sklearn.preprocessing.OneHotEncoder
        a Encoder to One-Hot encode categorical attributes

    Methods
    ----------
    preprocess_train_data(train_data)
        Preprocess the train data
    preprocess_test_data(test_data)
        Preprocess the test data
    """

    def __init__(self, categorical_columns, numerical_columns, categories) :
        """
        Parameters
        ----------
        categorical_columns : list
            a list with the categorical columns names of the dataframe
        numerical_columns : list
            a list with the numerical columns names of the dataframe
        categories : list
            a list of arrays especifying the categories of each categorical attribute to one-hot encode categorical attributes
        """
        
        self.numerical_columns = numerical_columns
        self.categorical_columns = categorical_columns
        self.label_encoders = dict()
        self.num_scaler = StandardScaler()
        self.onehot_encoder = OneHotEncoder(categories=categories)
    
    def preprocess_train_data(self, train_data) :
        """Preprocess the train data
    
        Parameters
        ----------
        train_data : pandas.core.frame.DataFrame
            the train dataframe to be preprocessed

        Returns
        -------
        numpy.ndarray
            a matrix with the preprocessed data
        """

        if self.categorical_columns is not None :

            #one-hot encode categorical attributes
            categorical_vars = train_data[self.categorical_columns]
            for col in categorical_vars.columns :
                categorical_vars[col] = categorical_vars[col].astype('category').cat.codes
            scaled_cat = self.onehot_encoder.fit_transform(categorical_vars).toarray()

            #standardize numerical attributes
            scaled_num = self.num_scaler.fit_transform(train_data[self.numerical_columns])

            #return the standardized numerical attributes stacked with the one-hot encoded categorical attributes
            return np.column_stack((scaled_num, scaled_cat))

        else : 

            #standardize numerical attributes
            return self.num_scaler.fit_transform(train_data[self.numerical_columns])

    
    def preprocess_test_data(self, test_data) :
        """Preprocess the test data
    
        Parameters
        ----------
        test_data : pandas.core.frame.DataFrame
            the test dataframe to be preprocessed

        Returns
        -------
        numpy.ndarray
            a matrix with the preprocessed data
        """

        if self.categorical_columns is not None :

            #one-hot encode categorical variables
            categorical_vars = test_data[self.categorical_columns]
            for col in categorical_vars.columns :
                categorical_vars[col] = categorical_vars[col].astype('category').cat.codes
            scaled_cat = self.onehot_encoder.transform(categorical_vars).toarray()

            #standardize numerical attributes
            scaled_num = self.num_scaler.transform(test_data[self.numerical_columns])

            #return the standardized numerical attributes stacked with the one-hot encoded categorical attributes
            return np.column_stack((scaled_num, scaled_cat))

        else :
            return self.num_scaler.transform(test_data[self.numerical_columns])


def train_evaluate_model_regressor(model_name, x_train, y_train, x_test, y_test) :
    """Train and evaluate a classifier model
    
    Parameters
    ----------
    model_name : string
        the name of the model
    x_train : numpy.ndarray
        training data
    y_train : numpy.ndarray
        training labels
    x_test : numpy.ndarray
        testing data
    y_test : numpy.ndarray
        testing labels

    Returns
    -------
    pandas.core.frame.DataFrame
        a dataframe with the obtained classification metrics
    """

    #initialize the model 
    model = initialize_model(model_name)

    #train the model
    model.fit(x_train, y_train)

    #make predictions
    predictions = model.predict(x_test)

    #compute metrics
    model_results = pd.DataFrame([[model_name,  np.round(mean_absolute_error(y_test, predictions),4),
                                    np.round(mean_squared_error(y_test, predictions),4),
                                    np.round(r2_score(y_test, predictions),4)]],
                                    columns=['Model', 'MAE', 'MSE', 'R2'])
    

    #return metric values
    return model_results


def initialize_model(model_name) :
    """Get the desired classifier model initialized.
    
    Parameters
    ----------
    model_name : string
        the name of the model

    Returns
    -------
    classifier_model
        the especified classifier model initialized
    """

    #define a dict that initialize each classifier model
    switcher = {
        'SVR': SVR(),
        'RandomForestRegressor': RandomForestRegressor(),
        'KNeighborsRegressor': KNeighborsRegressor(),
        'MLPRegressor': MLPRegressor(),
        'DecisionTreeRegressor': DecisionTreeRegressor()
    }

    #return the desired classifier model
    return switcher.get(model_name, "Invalid model name")