import numpy as np
import pandas as pd
import snsynth 
from snsynth.transform.table import TableTransformer
from snsynth.transform import *


class Preprocessor :
    """
    Class used to preprocess the data before the evaluation of the epsilon value.
    Considering transforming the data for using the synthetizers
    """    
    
    def __init__(self, data):
              
        self.tdata = data
        
        #initialize the list of int, float and categorical attributes
        self.int_features = data.select_dtypes(include=['int64']).columns.tolist()
        self.float_features = data.select_dtypes(include=['float64']).columns.tolist()
        self.continuous_features = data.select_dtypes(include=['int64','float64']).columns.tolist()
        self.categorical_columns = data.select_dtypes(include=['category','object']).columns.tolist()
        self.bins = 20
        self.train_data = data

        #initialize the dictionaries that will the  contrainsts for transforming the data without using privacy budget
        self.gan_constraints = {}
        self.cube_constraints = {}
        
        
    
    
    def build_transformers(self):
        """This function will build the transformers for the data, 
        and build the contrainsts using the attributes ob the class
        """
        #Initialize variables tc and tg 
        tc = None
        tg = None
        if self.continuous_features is not None:
            for column in self.continuous_features:
                self.cube_constraints[column] = BinTransformer(bins=self.bins, lower = np.min(self.train_data[column]), upper = np.max(self.train_data[column]))
                self.gan_constraints[column] =  MinMaxTransformer(lower = np.min(self.train_data[column]), upper = np.max(self.train_data[column]))
        
        if self.categorical_columns is not None:
            for column in self.categorical_columns:
                self.gan_constraints[column]= ChainTransformer([LabelTransformer(), OneHotEncoder()])
        
        #Building up transformers
        #consider when there are not continuous features
                
        tc= TableTransformer.create(
            data=self.train_data,
            style='cube',
            constraints=self.cube_constraints          
        )        
        
        tg =TableTransformer.create(
            data=self.train_data,
            style='gan',
            constraints=self.gan_constraints
        )
        
        return tc, tg
    
    def transformer_fit(self):
        """Fit the transformers to the data
        """
        tc, tg = self.build_transformers()
        tc.fit(self.train_data)
        tg.fit(self.train_data)
        
        #Notifying the user that the transformers were fitted
        print('Transformers were fitted')
        return tc, tg
    
    def build_dataTypes(self):
        """Build the data types for the data
        """
        data_types = {}
        for column in self.tdata.columns:
            if column in self.categorical_columns:
                data_types[column] = 'categorical'
            else:
                data_types[column] = 'interval'
        return data_types
    
    def build_auxiliar_columns(self):
        """Build the auxiliar columns for the data
        Considering half of the columns for one array and the other half for the other array
        """
        arr1 = self.tdata.columns[:len(self.tdata.columns)//2]
        arr2 = self.tdata.columns[len(self.tdata.columns)//2:]
        aux_cols = [arr1, arr2]
            
                
        return aux_cols
        
        
        
        
        
        