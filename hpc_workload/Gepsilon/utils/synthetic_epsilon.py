#This script will generate the synthetic data using different epsilon iterations
import warnings
warnings.filterwarnings("ignore")

import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from snsynth import Synthesizer
import json
import dill

def modeling_epsilon(data,tc,tg,categorical_columns,continuous_columns,path_output):
    
    #Generate a dictionary with the results 
    
    epsilon = 0.2
    results_dict = {}
    results_dict_pd={}
    
    while epsilon <= 5:
        print(f'This iteration is training with a privacy budget of {epsilon}')

       
        #DPCTGAN
        # synth=Synthesizer.create("dpctgan", epsilon=epsilon,verbose=True)
        # synth.fit(data,transformer=tg,
        #           categorical_columns=categorical_columns,
        #           continuous_columns=continuous_columns,
        #           preprocessor_eps=0.0)
        # DPCTGAN_samples=synth.sample(len(data))
        # DPCTGAN_samples.to_csv(os.path.join(path_output,f'epsilon_{epsilon}_CTGAN.csv'),index=False)
        
        #PATECTGAN
        # synth = Synthesizer.create("patectgan", epsilon=epsilon, verbose=True)
        # synth.fit(data,transformer=tg, preprocessor_eps=0.0)
        # PATECTGAN_samples = synth.sample(len(data))
        # PATECTGAN_samples.to_csv(os.path.join(path_output,f'epsilon_{epsilon}_PATECTGAN.csv'),index=False)

        #PACSYNTH
        # synth = Synthesizer.create("pacsynth", epsilon=epsilon, verbose=True)
        # synth.fit(data, preprocessor_eps=1)
        # pac_synth_samples= synth.sample(len(data))
        # pac_synth_samples.to_csv(os.path.join(path_output,f'epsilon_{epsilon}_PACSYNTH.csv'),index=False)

        #MST
        # synth = Synthesizer.create("mst", epsilon=epsilon, verbose=True)
        # synth.fit(data, transformer=tc,preprocessor_eps=0.0)
        # mst_samples = synth.sample(len(data))
        # mst_samples.to_csv(os.path.join(path_output,f'epsilon_{epsilon}_MST.csv'),index=False)
        
        #AIM
        synth = Synthesizer.create("aim", epsilon=epsilon, verbose=True)
        synth.fit(data,transformer=tc, preprocessor_eps=0.0)
        aim_samples = synth.sample(len(data))
        aim_samples.to_csv(os.path.join(path_output,f'epsilon_{epsilon}_AIM.csv'),index=False)
        
        #MWEM
        # synth = Synthesizer.create("mwem", epsilon=epsilon, verbose=True)
        # synth.fit(data,transformer=tc, preprocessor_eps=0.0)
        # mwem_samples = synth.sample(len(data))
        # mwem_samples.to_csv(os.path.join(path_output,f'epsilon_{epsilon}_MWEM.csv'),index=False)
        
        #DPGAN
        # synth = Synthesizer.create("dpgan", epsilon=epsilon)
        # synth.fit(data,transformer=tg, preprocessor_eps=0.0)
        # dpgan_samples = synth.sample(len(data))
        # dpgan_samples.to_csv(os.path.join(path_output,f'epsilon_{epsilon}_DPGAN.csv'),index=False)
        
        #PATEGAN
        # synth = Synthesizer.create("pategan", epsilon=epsilon)
        # synth.fit(data,transformer=tg, preprocessor_eps=0.0)
        # pategan_samples = synth.sample(len(data))
        # pategan_samples.to_csv(os.path.join(path_output,f'epsilon_{epsilon}_PATEGAN.csv'),index=False)
        
        
        # # Convert DataFrame to JSON string
        results_dict[f'epsilon_{epsilon}']=aim_samples.to_json()
        results_dict_pd[f'epsilon_{epsilon}']=aim_samples
        
        
        #Saving model 
        with open(os.path.join(path_output, f'model_epsilon_{epsilon}.pickle'), 'wb') as f:
            dill.dump(synth,f)
        
        print('(----------------------------------------------------)')
        epsilon += 0.5
        
    # Save the dictionary as a JSON file
    with open(os.path.join(path_output, 'results_AIM.json'), 'w') as f:
        json.dump(results_dict, f)
    
    return results_dict_pd
