#!/bin/bash
#SBATCH --job-name=AIM_bands
#SBATCH --ntasks=1
#SBATCH --time=20:00:00
#SBATCH --partition=compute
#SBATCH --mem=16GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=paosorio@vicomtech.org
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err

module load Miniconda3/4.9.2
conda create --name sns_env -y 
source activate  sns_env
pip install -r requirements.txt

srun python main_gep_bands.py