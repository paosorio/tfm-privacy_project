#Main just for running G_epsilon for a given dict jason with the synthetic data
import warnings
warnings.filterwarnings("ignore")


from utils.preprocessing import Preprocessor
from utils.synthetic_epsilon import modeling_epsilon
import pandas as pd 
import os 
import numpy as np 
import snsynth as sn
from utils.G_epsilon import get_Gepsilon_dict
from utils.G_epsilon import plot_gepsilon
from utils.G_epsilon import get_utility_matrix
import json
import dill


def generate_from_pkl(path_folder,n):
    #This method will generate a dictionary with the dataframes from the pkl files inside the folder
    #the format of the pkl files should be model_epsilon_{epsilon}.pickle
    #For every epsilon, there should be a pkl file. 
    data_dict={}
    #Loading the model with dill
    for file in os.listdir(path_folder):
        if file.endswith(".pickle"):
            with open(os.path.join(path_folder,file), 'rb') as f:
                model=dill.load(f)
                f.close()
            epsilon=file.split("_")[2][0:3]
            samples=model.sample(n)
            data_dict[epsilon]=samples
    return data_dict



def main():
    data = pd.read_csv("data/adult_original.csv")
    preprocessor = Preprocessor(data)
    data_types=preprocessor.build_dataTypes()

    print("Data transformed")
    k=10
    path_output = "results/general_bands/"    
    
    #We are going to generate the utility matrix for the original data in order to save computation time
    print("Generating utility matrix for the original data")
    matrix_Dp_c, matrix_Dp_r = get_utility_matrix(preprocessor.tdata, preprocessor.categorical_columns, 
                                                  preprocessor.continuous_features, data_types)
    
    print("Utility matrix generated")
    print("Starting to generate the epsilon bands")
    for i in range(k):
    
        data_AIM= generate_from_pkl("results/AIM_samples/",10000)
        data_DPCTGAN= generate_from_pkl("results/DPCTGAN_samples/",10000)
        # data_DPGAN= generate_from_pkl("results/DPGAN_samples/",10000)
        data_MST= generate_from_pkl("results/MST_samples/",10000)
        data_PATECTGAN= generate_from_pkl("results/PATECTGAN_samples/",10000)
        data_PATEGAN= generate_from_pkl("results/PATEGAN_samples/",10000)
        
        data_cat = [data_AIM,data_DPCTGAN,data_MST,data_PATECTGAN,data_PATEGAN]
        models=["AIM","DPCTGAN","DPGAN","MST","PATECTGAN","PATEGAN"]
    
        #Run the G_epsilon script for each dict
       
        print(f"Creating output folder in {path_output}{i}")
        os.makedirs(f"{path_output}{i}", exist_ok=True)
        print("Output folder created")
        
        for j in range(len(data_cat)):
            print(f"Running G_epsilon for data_{j}")
            #Calculate the g_epsilon
            #Create the output folder
            os.makedirs(f"{path_output}{i}/{models[j]}", exist_ok=True)
            gepsilon,mu,delta_t,delta_c,delta_r = get_Gepsilon_dict(path_output=f"{path_output}{i}/{models[j]}",data_=data_cat[j],Dp=preprocessor.tdata,
                                                                    matrix_Dp_c=matrix_Dp_c, matrix_Dp_r=matrix_Dp_r,
                                                                    interval_cols=preprocessor.continuous_features,categorical_cols=preprocessor.categorical_columns,
                                                                    data_types=data_types,alpha=1,beta=1)            
        
        
    # plot_gepsilon(gepsilon,mu,delta_t,delta_c,delta_r,'PATEGAN',path_output)
    print("G_epsilon plot saved")   
    

main()

