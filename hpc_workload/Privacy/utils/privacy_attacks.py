import warnings
warnings.filterwarnings("ignore")
import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import numpy.typing as npt


from anonymeter.evaluators import SinglingOutEvaluator
from anonymeter.evaluators import LinkabilityEvaluator
from anonymeter.evaluators import InferenceEvaluator
from .MIA import MembershipInferenceEvaluator

#method for running every attack (singlingOutEvaluator-univariate, SinglingOutEvaluator-multi, linkabilityEvaluator,and InferenceEvaluator) with a synthetic, original, control dataset, n_attacks, aux_cols, and output must be a pd_DataFrame with theattack_rate, baseline_rate, and control_rate, and plotting the risk per column in the inference attack.
def run_all_attacks(ori, syn, control, n_attacks, aux_cols, model):
    attack_rate_ = []
    baseline_rate_ = []
    control_rate_ = []
    risk_eval_=[]
    
    #SinglingOutEvaluator-univariate
    evaluator = SinglingOutEvaluator(ori=ori, syn=syn, control=control, n_attacks=n_attacks)
    try:
        evaluator.evaluate(mode='univariate')
        risk_ = evaluator.risk()
        res = evaluator.results()
        attack_rate_.append(res.attack_rate)
        baseline_rate_.append(res.baseline_rate)
        control_rate_.append(res.control_rate)
        risk_eval_.append(risk_)
        

    except RuntimeError as ex: 
        print(f"Singling out evaluation failed with {ex}. Please re-run this cell."
          "For more stable results increase `n_attacks`. Note that this will "
          "make the evaluation slower.")
        attack_rate_.append(0)
        baseline_rate_.append(0)
        control_rate_.append(0)
        risk_eval_.append(0)
    
   
    
    
    #SinglingOutEvaluator-multi
    evaluator = SinglingOutEvaluator(ori=ori, syn=syn, control=control, n_attacks=n_attacks)
  
    try:
        evaluator.evaluate(mode='multivariate')
        risk_ = evaluator.risk()
        res = evaluator.results()
        attack_rate_.append(res.attack_rate)
        baseline_rate_.append(res.baseline_rate)
        control_rate_.append(res.control_rate)
        risk_eval_.append(risk_)

    except RuntimeError as ex: 
        print(f"Singling out evaluation failed with {ex}. Please re-run this cell."
          "For more stable results increase `n_attacks`. Note that this will "
          "make the evaluation slower.")
        attack_rate_.append(0)
        baseline_rate_.append(0)
        control_rate_.append(0) 
        risk_eval_.append(0)
    
    
    
    
    #LinkabilityEvaluator
    evaluator = LinkabilityEvaluator(ori=ori, syn=syn, control=control, n_attacks=n_attacks, aux_cols=aux_cols)
    evaluator.evaluate(n_jobs=-2)
    risk_ = evaluator.risk()
    res = evaluator.results()
    attack_rate_.append(res.attack_rate)
    baseline_rate_.append(res.baseline_rate)
    control_rate_.append(res.control_rate)
    risk_eval_.append(risk_)
    
    
    #MembershipInferenceEvaluator
    evaluator = MembershipInferenceEvaluator(ori=ori, syn=syn, control=control, tol=0.10, n_attacks=n_attacks)
    
    try:
        evaluator.evaluate()
        risk_ = evaluator.risk()
        res = evaluator.results()
        attack_rate_.append(res.attack_rate)
        baseline_rate_.append(res.baseline_rate)
        control_rate_.append(res.control_rate)
        risk_eval_.append(risk_)
        
    except RuntimeError as ex: 
        print(f"Membership Inference Attack evaluation failed with {ex}. Please re-run this cell."
            "For more stable results increase `n_attacks`. Note that this will "
            "make the evaluation slower.")
        attack_rate_.append(0)
        baseline_rate_.append(0)
        control_rate_.append(0)
        risk_eval_.append(0)




    #InferenceEvaluator
    results = []
    columns = ori.columns
    for secret in columns:
        aux_cols = [col for col in columns if col != secret]
        evaluator = InferenceEvaluator(ori=ori, syn=syn, control=control, aux_cols=aux_cols, secret=secret, n_attacks=n_attacks)
        evaluator.evaluate(n_jobs=-2)
        results.append((secret, evaluator.results()))
    risks = [res[1].risk().value for res in results]
    columns = [res[0] for res in results]
    
    # fig, ax = plt.subplots()
    # ax.bar(x=columns, height=risks, alpha=0.5, ecolor='black', capsize=10)
    # plt.xticks(rotation=45, ha='right')
    # plt.title(f"Inference risk for {model}")
    # ax.set_ylabel("Measured inference risk")
    # _ = ax.set_xlabel("Secret column")  
    
    
    # filename = f'Inference_risk_{model}.png'
    # plt.savefig(filename, bbox_inches='tight')  # Save as PNG, bbox_inches='tight' removes extra white spaces
    # plt.close()  # Close
    
    return pd.DataFrame({'attack_rate ': attack_rate_, 'baseline_rate': baseline_rate_, 'control_rate': control_rate_,'Risk':risk_eval_}, index=['SinglingOutEvaluator-univariate', 'SinglingOutEvaluator-multi', 'LinkabilityEvaluator','MembershipInferenceEvaluator']),pd.DataFrame({'Secret':columns, 'Risk':risks})

