
import pandas as pd
import numpy as np
from typing import List, Optional
from math import fabs, isnan


#Creating a privacy evaluator to perform Membership Inference Attack from scratch
from typing import List, Tuple
from anonymeter.neighbors.mixed_types_kneighbors import MixedTypeKNeighbors
from anonymeter.stats.confidence import EvaluationResults, PrivacyRisk
from anonymeter.preprocessing.transformations import mixed_types_transform 
from anonymeter.preprocessing.type_detection import detect_consistent_col_types



def gower_distance(x: np.ndarray, y: np.ndarray, cat_cols_index: int) -> float:
    """
    Calculate the Gower distance between two rows.
    """
    #Calculate the distance for every row in the target dataset
    
    
    dist = 0.0

    for i in range(len(x)):
        if isnan(x[i]) and isnan(y[i]):
            dist += 1

        else:
            if i < cat_cols_index:
                dist += fabs(x[i] - y[i])

            else:
                if x[i] != y[i]:
                    dist += 1
    return dist

def _run_attack(
    ori: pd.DataFrame,
    target: pd.DataFrame,
    syn: pd.DataFrame,   
    n_attacks: int,
    naive: bool,
    tol: float
)-> int:
    targets = target.sample(n=n_attacks, replace=False)
    
    if naive:
        guesses = syn.sample(n=n_attacks, replace=False)
        c_types= detect_consistent_col_types(df1=targets, df2=guesses)
        target_trans,guesses_trans = mixed_types_transform(df1=targets,
                                                           df2=guesses,
                                                           num_cols=c_types["num"], 
                                                           cat_cols=c_types["cat"])
        
        #Calculate the distance for every row in the target dataset
        
        
        dist_xy=np.zeros(len(targets),dtype=np.float64)
        for i in range(len(targets)):
            dist_xy[i]=gower_distance(target_trans.iloc[i],guesses_trans.iloc[i],cat_cols_index=len(c_types['num']))
            
        distances= pd.Series(dist_xy).values
        
    else:
        knn = MixedTypeKNeighbors(n_neighbors=1)
        knn.fit(syn)
        distances ,guesses_idx = knn.kneighbors(queries=targets,return_distance= True)
        guesses= syn.iloc[guesses_idx.flatten()]
        
        
    return evaluate_inference_guesses(
        guesses=guesses,distances=distances,tolerance=tol).sum()
    

def evaluate_inference_guesses(guesses: pd.Series, 
                               distances: pd.Series,
                               tolerance: float = 0.05) -> pd.Series:
    """
    Evaluate the membership inference guesses.
    If the distance is less than the tolerance, the guess is correct.
    """
    return (distances < tolerance).astype(int)

class MembershipInferenceEvaluator:
    def __init__(self, 
                 ori: pd.DataFrame, 
                 syn: pd.DataFrame, 
                 control: pd.DataFrame, 
                 tol: float,
                 n_attacks: int):
        self.ori = ori
        self.syn = syn
        self.control = control
        self.n_attacks = n_attacks
        self.tol = tol


    def _attack(self, target: pd.DataFrame, naive: bool) -> int:
        return _run_attack(
            ori=self.ori,
            target=target,
            syn=self.syn,
            n_attacks=self.n_attacks,
            naive=naive,
            tol=self.tol
        )
    
    def evaluate(self):
        self._n_baseline = self._attack(target=self.ori, naive=True)
        self._n_success = self._attack(target=self.ori, naive=False)
        self._n_control = self._attack(target=self.control, naive=False)
        
    def results(self, confidence_level : float =0.95)->EvaluationResults:
        return EvaluationResults(
            n_attacks=self.n_attacks,
            n_success=self._n_success,
            n_baseline=self._n_baseline,
            n_control=self._n_control,
            confidence_level=confidence_level
            )
    
    def risk(self, confidence_level: float = 0.95, baseline: bool = False) -> PrivacyRisk:
        results=self.results(confidence_level)
        return results.risk(baseline)
        
    