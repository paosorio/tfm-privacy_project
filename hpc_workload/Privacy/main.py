#With this notebook, we will calculate the privacy attacks for all the databases generated for privacy models with different epsilons
import warnings
warnings.filterwarnings("ignore")

import os
import pandas as pd
import matplotlib.pyplot as plt
import json
from utils.privacy_attacks import run_all_attacks
from utils.preprocessing import Preprocessor

def main():
    #Load the data
    ori = pd.read_csv("data/adult_original.csv")
    control = pd.read_csv("data/adult_control.csv")
    
    #
    preproscesor= Preprocessor(ori)
    
    #Getting auxiliar columns
    aux_cols = preproscesor.build_auxiliar_columns()
    
    #The synthetic data is stored in different folders, with the format
    #model_samples/epsilon_value_model.csv. We will iterate over all the folders
    #We are going to save the results of every model in a dictionary
    #The key will be the epsilon value and the value will be a dictionary with the results of the attacks
    
        
    #Iterate over the folders to fill the dictionaries with the synthetic data. The data for every model
    #is stored in a different folder, and every folder has the synthetic data for different epsilon values
    # With the epsilon value in the file name 
    # Path to the main data folder
    data_folder = "data/"
    data_output = "results/"

    # Dictionary to store results
    results_attacks = {}
    results_inference = {}
    print("Initializing attack")
    # Iterate over each model folder
    for model_folder in os.listdir(data_folder):
        if model_folder.endswith("_samples"):
            model_name = model_folder.split("_")[0]
            model_path = os.path.join(data_folder, model_folder)
            
            # Dictionary to store results for this model
            model_results_attacks = {}
            model_results_inference = {}
            
                 
            print(f"Attacking SD from {model_name}")
            # Iterate over each CSV file in the model folder
            for csv_file in os.listdir(model_path):
                if csv_file.endswith(".csv"):
                    csv_path = os.path.join(model_path, csv_file)
                    
                    # Read CSV file into a DataFrame
                    df = pd.read_csv(csv_path)
                    
                    # Apply your function to the DataFrame
                    attack_rate,inference_risk = run_all_attacks(ori,df,control,500,aux_cols,model_name)
                 
                    
                    # Store result in the model's dictionary
                    epsilon = csv_file.split("_")[1]
                    model_results_attacks[epsilon] = attack_rate.to_json()
                    model_results_inference[epsilon] = inference_risk.to_json()
            
            # Store model's results in the main results dictionary
            print(type(model_results_attacks))
            print(model_results_attacks)

            results_attacks[model_name] = model_results_attacks
            results_inference[model_name] = model_results_inference            
            #Save both dictionaries in a json file
            with open(data_output+f"results_attacks_{model_name}.json", "w") as f:
                json.dump(model_results_attacks, f)
            with open(data_output+f"results_inference_{model_name}.json", "w") as f:
                json.dump(model_results_inference, f)
        
    print("Attacks finished")
    #Save dictionary in a json file
    with open(data_output+"results_attacks.json", "w") as f:
        json.dump(results_attacks, f)
    with open(data_output+"results_inference.json", "w") as f:
        json.dump(results_inference, f)

main()