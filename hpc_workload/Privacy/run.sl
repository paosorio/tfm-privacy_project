#!/bin/bash
#SBATCH --job-name=PrivAttack
#SBATCH --ntasks=1
#SBATCH --time=100:00:00
#SBATCH --partition=compute
#SBATCH --mem=32GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=paosorio@vicomtech.org
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err

module load Miniconda3/4.9.2
conda create --name priv_att -y 
source activate  priv_att
pip install -r requirements.txt

srun python main.py