import warnings
warnings.filterwarnings("ignore")
from utils.dd_plot import ddplot
import pandas as pd

from utils.preprocessing import Preprocessor



def main():
    n=10000
    data = pd.read_csv("data/adult_original.csv")
    print("Original data loaded")
    
    #Unprivate data
    FASTML= pd.read_csv("data/FASTML_samples.csv",index_col=0)
    CTGAN= pd.read_csv("data/CTGAN_samples.csv",index_col=0)
    GaussianCopula= pd.read_csv("data/GaussianCopula_samples.csv",index_col=0)
    TVAE= pd.read_csv("data/TVAE_samples.csv",index_col=0)
    CopulaGAN= pd.read_csv("data/CopulaGAN_samples.csv",index_col=0)
    non_param_copulas= pd.read_csv("data/non_param_Copulas_samples.csv",index_col=0)
    
    #Private data
    AIM= pd.read_csv("data/aim_samples_epsilon3.csv",index_col=0)
    DPCTGAN= pd.read_csv("data/DPCTGAN_samples_epsilon3.csv",index_col=0)
    DPGAN= pd.read_csv("data/DPGAN._samples_epsilon3.csv")
    PATECTGAN= pd.read_csv("data/PATECTGAN_samples_epsilon3.csv",index_col=0)
    PATEGAN= pd.read_csv("data/PATEGAN_samples_epsilon3.csv")
    MST= pd.read_csv("data/MST_samples_epsilon3.csv",index_col=0)
    
   
     #Select n samples from the data
    data = data.sample(n).reset_index(drop=True)
    FASTML = FASTML.sample(n).reset_index(drop=True)
    CTGAN = CTGAN.sample(n).reset_index(drop=True)
    GaussianCopula = GaussianCopula.sample(n).reset_index(drop=True)
    TVAE = TVAE.sample(n).reset_index(drop=True)
    CopulaGAN = CopulaGAN.sample(n).reset_index(drop=True)
    non_param_copulas = non_param_copulas.sample(n).reset_index(drop=True)
    
    AIM = AIM.sample(n).reset_index(drop=True)
    DPCTGAN = DPCTGAN.sample(n).reset_index(drop=True)
    DPGAN = DPGAN.sample(n).reset_index(drop=True)
    PATECTGAN = PATECTGAN.sample(n).reset_index(drop=True)
    PATEGAN = PATEGAN.sample(n).reset_index(drop=True)
    MST = MST.sample(n).reset_index(drop=True)
        
    
    #Preprocess the data
    X = Preprocessor(data)
    Y_FASTML = Preprocessor(FASTML)
    Y_CTGAN = Preprocessor(CTGAN)
    Y_GaussianCopula = Preprocessor(GaussianCopula)
    Y_TVAE = Preprocessor(TVAE)
    Y_CopulaGAN = Preprocessor(CopulaGAN)
    Y_non_param_copulas = Preprocessor(non_param_copulas)
    
    Y_AIM = Preprocessor(AIM)
    Y_DPCTGAN = Preprocessor(DPCTGAN)
    Y_DPGAN = Preprocessor(DPGAN)
    Y_PATECTGAN = Preprocessor(PATECTGAN)
    Y_PATEGAN = Preprocessor(PATEGAN)
    Y_MST = Preprocessor(MST)
    
    
    
    #Encode data
    X_encoded = X.encode_dataset()
    Y_FASTML_encoded = Y_FASTML.encode_dataset()
    Y_CTGAN_encoded = Y_CTGAN.encode_dataset()
    Y_GaussianCopula_encoded = Y_GaussianCopula.encode_dataset()
    Y_TVAE_encoded = Y_TVAE.encode_dataset()
    Y_CopulaGAN_encoded = Y_CopulaGAN.encode_dataset()
    Y_non_param_copulas_encoded = Y_non_param_copulas.encode_dataset()
    
    Y_AIM_encoded = Y_AIM.encode_dataset()
    Y_DPCTGAN_encoded = Y_DPCTGAN.encode_dataset()
    Y_DPGAN_encoded = Y_DPGAN.encode_dataset()
    Y_PATECTGAN_encoded = Y_PATECTGAN.encode_dataset()
    Y_PATEGAN_encoded = Y_PATEGAN.encode_dataset()
    Y_MST_encoded = Y_MST.encode_dataset()
    
    
    
    
    print("Data preprocessed")
    #Calculate the DD plot
    ddplot(X_encoded,Y_FASTML_encoded,'FASTML','outputs/')
    ddplot(X_encoded,Y_CTGAN_encoded,'CTGAN','outputs/')
    ddplot(X_encoded,Y_GaussianCopula_encoded,'GaussianCopula','outputs/')
    ddplot(X_encoded,Y_TVAE_encoded,'TVAE','outputs/')
    ddplot(X_encoded,Y_CopulaGAN_encoded,'CopulaGAN','outputs/')
    ddplot(X_encoded,Y_non_param_copulas_encoded,'non_param_copulas','outputs/')
    
    ddplot(X_encoded,Y_AIM_encoded,'AIM','outputs/')
    ddplot(X_encoded,Y_DPCTGAN_encoded,'DPCTGAN','outputs/')
    ddplot(X_encoded,Y_DPGAN_encoded,'DPGAN','outputs/')
    ddplot(X_encoded,Y_PATECTGAN_encoded,'PATECTGAN','outputs/')
    ddplot(X_encoded,Y_PATEGAN_encoded,'PATEGAN','outputs/')
    ddplot(X_encoded,Y_MST_encoded,'MST','outputs/')
    
    
    print("DD plots generated")
    
main()