import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def euclidean_distance(x, y):
    return np.sqrt(np.sum((x - y) ** 2))

def calculate_denominator(X,Y,Z):
    Z_distances_X = np.sum(np.array([[euclidean_distance(x, z) for x in X] for z in Z]), axis=1)
    den_X = np.sum(Z_distances_X)
    
    Z_distances_Y = np.sum(np.array([[euclidean_distance(y, z) for y in Y] for z in Z]), axis=1)
    den_Y = np.sum(Z_distances_Y)

    return den_X, den_Y, Z_distances_X, Z_distances_Y       

def calculate_DD(X,Y,Z):
    den_X, den_Y, Z_distances_X, Z_distances_Y = calculate_denominator(X,Y,Z)
    DF_n = 1 - (Z_distances_X / den_X)
    DG_m = 1 - (Z_distances_Y / den_Y)
    return DF_n, DG_m
     

def plot_DD_plot(Df_n, Df_m,algorithm,path):
    plt.figure(figsize=(10, 5))
    plt.scatter(Df_n, Df_m, color='blue')
    plt.xlabel('Real Data')
    plt.ylabel('Synthetic Data')
    plt.title(f'DD-plot for {algorithm}')
    #saving the plot
    plt.savefig(path+f'{algorithm}.png')
    plt.show()

def ddplot(df_X, df_Y,algorithm,path):
    df_Z= pd.concat([df_X,df_Y],axis=0,ignore_index=True)
    X= df_X.values
    Y= df_Y.values
    Z= df_Z.values
    Df_n, DG_m = calculate_DD(X, Y, Z)
    #Save Df_n and DG_m together as a csv file
    DD_data = pd.DataFrame({'Df_n': Df_n, 'DG_m': DG_m})
    #Save in path 
    DD_data.to_csv(path+f'{algorithm}_DD_data.csv', index=False)
    plot_DD_plot(Df_n, DG_m,algorithm, path)
    return Df_n, DG_m