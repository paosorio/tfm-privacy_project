import numpy as np
import pandas as pd



class Preprocessor :
    """
    Class used to preprocess the data before the evaluation of the epsilon value.
    Considering transforming the data for using the synthetizers
    """    
    
    def __init__(self, data):
              
        self.tdata = data
        
        #initialize the list of int, float and categorical attributes
        self.int_features = data.select_dtypes(include=['int64']).columns.tolist()
        self.float_features = data.select_dtypes(include=['float64']).columns.tolist()
        self.continuous_features = data.select_dtypes(include=['int64','float64']).columns.tolist()
        self.categorical_columns = data.select_dtypes(include=['category','object']).columns.tolist()
        self.bins = 20
        self.train_data = data

 
        
    def build_dataTypes(self):
        """Build the data types for the data
        """
        data_types = {}
        for column in self.tdata.columns:
            if column in self.categorical_columns:
                data_types[column] = 'categorical'
            else:
                data_types[column] = 'interval'
        return data_types
    
    def encode_dataset(self):
        data_encoded=self.tdata.copy()
        for col in self.categorical_columns:
            data_encoded[col]=encode_categorical(data_encoded,col)
        return data_encoded
        
    
def encode_categorical(df: pd.DataFrame, column: str) -> pd.DataFrame:
    count=df[column].value_counts().sort_values(ascending=False)
    cum_sum=count.cumsum()
    cum_perc=count.cumsum()/count.sum()
    #Getting intervals
    intervals=[]
    for i in range(len(cum_perc)):
        if i==0:
            intervals.append((0,cum_perc.iloc[i]))
        else:
            intervals.append((cum_perc.iloc[i-1],cum_perc.iloc[i]))
    
    # print(column + ' has the following distribution:')
    # print(count)
    # print(intervals)
    
    temp_categorical=[]
    cat_var= df[column]
    for i in range(len(df)):
        #Find the interval that corresponds to the category
        idx=np.where(cat_var[i]==np.array(count.index))[0][0]
        a,b=intervals[idx]
        #Choose a value between a and b by sampling from a Truncated Gaussian distribution, with miu at the center and delta = (b-a)/6
        miu=(a+b)/2
        delta=(b-a)/6
        temp_categorical.append(np.random.normal(miu,delta,1)[0])
        
                
    return temp_categorical    
        
        
        
        
        