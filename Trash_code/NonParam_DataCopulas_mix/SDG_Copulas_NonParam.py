import warnings
warnings.filterwarnings('ignore')

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns


from NonParametricCopulas.SyntheticDataCopulas.src.synthetic_data_generator import generate_multivariate_data
from NonParametricCopulas.SyntheticDataCopulas.src.plot_comparision import scatter
import random as rd

def encode_categorical(df: pd.DataFrame, column: str, plotting=False) -> pd.DataFrame:
    count=df[column].value_counts().sort_values(ascending=False)
    cum_sum=count.cumsum()
    cum_perc=count.cumsum()/count.sum()
    #Getting intervals
    intervals=[]
    for i in range(len(cum_perc)):
        if i==0:
            intervals.append((0,cum_perc.iloc[i]))
        else:
            intervals.append((cum_perc.iloc[i-1],cum_perc.iloc[i]))
    
    temp_categorical=[]
    cat_var= df[column]
    for i in range(len(df)):
        #Find the interval that corresponds to the category
        idx=np.where(cat_var[i]==np.array(count.index))[0][0]
        a,b=intervals[idx]
        #Choose a value between a and b by sampling from a Truncated Gaussian distribution, with miu at the center and delta = (b-a)/6
        miu=(a+b)/2
        delta=(b-a)/6
        temp_categorical.append(np.random.normal(miu,delta,1)[0])
        
    if plotting:
        plt.figure(figsize=(10,5))
        plt.subplot(1,2,1)
        plt.bar(count.index,count.values)
        #Put the x labels vertically
        plt.xticks(rotation=90)
        plt.title('Original')
        plt.subplot(1,2,2)
        plt.hist(temp_categorical,bins=30)
        plt.title('Encoded')
        plt.show()
                
    return temp_categorical


def plot_comparision(df_original: pd.DataFrame, df_synthetic: pd.DataFrame, categorical_columns: list) -> pd.DataFrame:
    #sns pairplot between real and synthetic data to compare distributions
    df_original['Type'] = 'real'
    df_synthetic['Type'] = 'synthetic'

    df_comparison = pd.concat([df_original, df_synthetic]).reset_index(drop=True)


    colors = ["#4374B3", "#FF0B04"]
    sns.set_palette(sns.color_palette(colors))

    sns.pairplot(df_comparison, hue="Type", diag_kind="kde", corner=False,
                diag_kws={'common_norm': False}, plot_kws={'markers': ['o', 'v'], 'alpha': 0.7})
    
    num_categories = len(categorical_columns)

    for i, col in enumerate(categorical_columns):
        for j, col2 in enumerate(categorical_columns):
            if j > i:
                break
            elif i != j:
                sns.catplot(x=col, y=col2,hue="Type", data=df_comparison, )
                plt.xticks(rotation=90)
                plt.show
            else:
                sns.catplot(x=col, hue="Type",kind="count", data=df_comparison, palette="Set1")
                plt.xticks(rotation=90)
                plt.show()
                
    return None
    
    
    
    

def decode_categorical(df: pd.DataFrame, column: str, encoded_column) -> pd.DataFrame:
    cat_decoded=[]
    count=df[column].value_counts().sort_values(ascending=False)
    cum_sum=count.cumsum()
    cum_perc=count.cumsum()/count.sum()
    #Getting intervals
    intervals=[]
    for i in range(len(cum_perc)):
        if i==0:
            intervals.append((0,cum_perc.iloc[i]))
        else:
            intervals.append((cum_perc.iloc[i-1],cum_perc.iloc[i]))


    for i in range(len(encoded_column)):
        for j in range(len(intervals)):
            a,b=intervals[j]
            if encoded_column[i]>=a and encoded_column[i]<=b:
                cat_decoded.append(count.index[j])
                break
            if j==len(intervals)-1:
                #This happen when the value is out of the range of the interval [0,1]
                #Assign to j=0 if the value is less than 0, or j=len(intervals)-1 if the value is greater than 1
                if encoded_column[i]<0:
                    cat_decoded.append(count.index[0])
                else:
                    cat_decoded.append(count.index[len(intervals)-1])
            
    return cat_decoded

def generate_multivariate_data_mix(df: pd.DataFrame, cat_columns:list, num_columns:list, samples:int, 
                                   plotting=False,  bins=20) -> pd.DataFrame:
    df_encoded=df.copy()
    for col in cat_columns:
        df_encoded[col]=encode_categorical(df_encoded,col,plotting)
    
    df_encoded_synthetic=generate_multivariate_data(df_encoded,bins, samples)
    
    df_decoded=df_encoded_synthetic.copy()
    
    for col in cat_columns:
        df_decoded[col]=decode_categorical(df,col,df_decoded[col])
        
    if plotting:
        plot_comparision(df,df_decoded,cat_columns)
    
    return df_decoded  



