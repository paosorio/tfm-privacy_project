import warnings 
warnings.filterwarnings("ignore")

import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from snsynth import Synthesizer



data=pd.read_csv('Results/Synthetic Data/Adult_preprocessed/adult.csv')
categorical_columns = data.select_dtypes(include=['object']).columns
categorical_columns = categorical_columns.tolist()
continuous_columns = data.select_dtypes(include=['float64','int64']).columns
continuous_columns = continuous_columns.tolist()
print(categorical_columns)
print(continuous_columns)

#Implementing different algorithms for generating synthetic data
#Algorithm 1: AIM

#Implementing different algorithms for generating synthetic data
#Algorithm 1: DPCTGAN
# synth = Synthesizer.create("dpctgan", epsilon=3,verbose=True)
# synth.fit(data,categorical_columns=categorical_columns,continuous_columns=continuous_columns,preprocessor_eps=1)
# data_ctgan=synth.sample(len(data))
synth = Synthesizer.create("patectgan", epsilon=3.0, verbose=True)
synth.fit(data, preprocessor_eps=1.0)
pums_synth = synth.sample(1000)